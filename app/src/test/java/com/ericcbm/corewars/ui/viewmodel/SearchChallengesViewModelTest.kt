package com.ericcbm.corewars.ui.viewmodel

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.corewars.CodeWarsRestApi
import com.ericcbm.corewars.RxImmediateSchedulerRule
import com.ericcbm.corewars.model.*
import io.reactivex.Observable
import org.junit.*
import org.junit.runner.RunWith

import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchChallengesViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val schedulers = RxImmediateSchedulerRule()

    @Mock
    private lateinit var codeWarsRestApi: CodeWarsRestApi
    @Mock
    private lateinit var challengesObserver: Observer<Pair<List<Challenge>?, Boolean>>
    @Mock
    private lateinit var errorMessageObserver: Observer<String>
    private var viewModel: SearchChallengesViewModel? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = SearchChallengesViewModel()
        viewModel?.challengesList?.observeForever(challengesObserver)
        viewModel?.errorMessage?.observeForever(errorMessageObserver)
    }

    @After
    fun tearDown() {
        viewModel = null
    }

    @Test
    fun getAuthoredChallengesNull() {
        Mockito.`when`(codeWarsRestApi.getAuthoredChallenges(""))
            .thenReturn(null)
        viewModel?.getAuthoredChallenges("")
        Mockito.verify(challengesObserver).onChanged(Pair(ArrayList(), false))
    }

    @Test
    fun getCompletedChallengesNull() {
        Mockito.`when`(codeWarsRestApi.getCompletedChallenges("", 0))
            .thenReturn(null)
        viewModel?.getCompletedChallenges("")
        Mockito.verify(challengesObserver).onChanged(Pair(ArrayList(), false))
    }

    @Test
    fun getAuthoredChallengesError() {
        val throwable = Resources.NotFoundException("error")
        Mockito.`when`(codeWarsRestApi.getAuthoredChallenges(""))
            .thenReturn(Observable.error(throwable))
        viewModel?.getAuthoredChallenges("")
        Mockito.verify(errorMessageObserver).onChanged(throwable.localizedMessage)
    }

    @Test
    fun getCompletedChallengesError() {
        val throwable = Resources.NotFoundException("error")
        Mockito.`when`(codeWarsRestApi.getCompletedChallenges("", 0))
            .thenReturn(Observable.error(throwable))
        viewModel?.getCompletedChallenges("")
        Mockito.verify(errorMessageObserver).onChanged(throwable.localizedMessage)
    }

    @Test
    fun getAuthoredChallengesSuccess() {
        val data = ArrayList<AuthoredChallenge>()
        val authoredChallengesList = AuthoredChallengesList(data, 0, 0)
        Mockito.`when`(codeWarsRestApi.getAuthoredChallenges(""))
            .thenReturn(Observable.just(authoredChallengesList))
        viewModel?.getAuthoredChallenges("")
        Mockito.verify(challengesObserver).onChanged(Pair(data, false))
    }

    @Test
    fun getCompletedChallengesSuccess() {
        val data = ArrayList<CompletedChallenge>()
        val completedChallengesList = CompletedChallengesList(data, 0, 0)
        Mockito.`when`(codeWarsRestApi.getCompletedChallenges("", 0))
            .thenReturn(Observable.just(completedChallengesList))
        viewModel?.getCompletedChallenges("")
        Mockito.verify(challengesObserver).onChanged(Pair(data, false))
    }

    @Test
    fun getCompletedChallengesSecondPageSuccess() {
        val data = ArrayList<CompletedChallenge>()
        val completedChallengesList = CompletedChallengesList(data, 1, 0)
        Mockito.`when`(codeWarsRestApi.getCompletedChallenges("", 1))
            .thenReturn(Observable.just(completedChallengesList))
        viewModel?.getCompletedChallenges("", 1)
        Mockito.verify(challengesObserver).onChanged(Pair(data, true))
    }
}