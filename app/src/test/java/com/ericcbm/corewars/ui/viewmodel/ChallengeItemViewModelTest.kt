package com.ericcbm.corewars.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.corewars.model.AuthoredChallenge
import com.ericcbm.corewars.model.Challenge
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class ChallengeItemViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<Challenge>
    private var viewModel: ChallengeItemViewModel? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = ChallengeItemViewModel()
        viewModel?.challenge?.observeForever(observer)
    }

    @After
    fun tearDown() {
        viewModel = null
    }

    @Test
    fun bind() {
        val challenge = AuthoredChallenge(
            "id", "name", "description",
            0, "rankName", ArrayList(), ArrayList()
        )
        viewModel?.bind(challenge)
        verify(observer).onChanged(challenge)
    }
}