package com.ericcbm.corewars.ui.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.corewars.CodeWarsRestApi
import com.ericcbm.corewars.RxImmediateSchedulerRule
import com.ericcbm.corewars.database.MemberDao
import com.ericcbm.corewars.model.Member
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SearchMemberViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val schedulers = RxImmediateSchedulerRule()

    @Mock
    private lateinit var application: Application
    @Mock
    private lateinit var codeWarsRestApi: CodeWarsRestApi
    @Mock
    private lateinit var memberDao: MemberDao
    @Mock
    private lateinit var memberObserver: Observer<Member>
    @Mock
    private lateinit var errorMessageObserver: Observer<String>
    private var viewModel: SearchMemberViewModel? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = SearchMemberViewModel(application)
        viewModel?.savedMember?.observeForever(memberObserver)
        viewModel?.errorMessage?.observeForever(errorMessageObserver)
    }

    @After
    fun tearDown() {
        viewModel = null
    }

    @Test
    fun searchMemberSuccess() {
        val member = Member(
            "username", "name",
            0, 0, null, "bestLanguage",
            0
        )
        Mockito.`when`(codeWarsRestApi.getMember("username"))
            .thenReturn(Observable.just(member))
        Mockito.`when`(memberDao.insert(member)).thenReturn(Completable.complete())
        viewModel?.searchMember("username")
        Mockito.verify(memberObserver).onChanged(member)
    }
}