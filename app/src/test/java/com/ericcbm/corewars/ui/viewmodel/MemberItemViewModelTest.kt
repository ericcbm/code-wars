package com.ericcbm.corewars.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.corewars.model.Member
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class MemberItemViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<Member>
    private var viewModel: MemberItemViewModel? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MemberItemViewModel()
        viewModel?.member?.observeForever(observer)
    }

    @After
    fun tearDown() {
        viewModel = null
    }

    @Test
    fun bind() {
        val member = Member(
            "username", "name", 0,
            0, null, "bestLanguage", 0
        )
        viewModel?.bind(member)
        verify(observer).onChanged(member)
    }
}