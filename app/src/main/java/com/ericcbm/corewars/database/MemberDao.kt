package com.ericcbm.corewars.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ericcbm.corewars.model.Member
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface MemberDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg member: Member): Completable

    @Query("SELECT * FROM member ORDER BY searchTimestamp DESC limit 5")
    fun getFiveLastSearched():  Maybe<List<Member>>
}