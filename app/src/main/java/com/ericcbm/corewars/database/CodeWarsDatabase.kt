package com.ericcbm.corewars.database

import android.content.Context
import androidx.room.*
import com.ericcbm.corewars.model.Member

@Database(entities = [Member::class], version = 1)
@TypeConverters(Converter::class)
abstract class CodeWarsDatabase : RoomDatabase() {
    abstract fun memberDao(): MemberDao

    companion object {
        private const val DATABASE_NAME: String = "code-wars"
        @Volatile
        private var INSTANCE: CodeWarsDatabase? = null

        fun getDatabase(context: Context): CodeWarsDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CodeWarsDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}