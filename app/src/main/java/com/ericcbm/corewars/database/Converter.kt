package com.ericcbm.corewars.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonObject

class Converter {
    @TypeConverter
    fun toString(jsonObject: JsonObject?): String? {
        return if (jsonObject == null) null else Gson().toJson(jsonObject)
    }

    @TypeConverter
    fun toJsonObject(string: String?): JsonObject? {
        return Gson().fromJson(string, JsonObject::class.java)
    }
}