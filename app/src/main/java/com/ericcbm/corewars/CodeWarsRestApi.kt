package com.ericcbm.corewars

import com.ericcbm.corewars.model.AuthoredChallengesList
import com.ericcbm.corewars.model.CompletedChallengesList
import com.ericcbm.corewars.model.Member
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface CodeWarsRestApi {

    @GET("api/v1/users/{username}")
    fun getMember(@Path("username") username: String) : Observable<Member>

    @GET("api/v1/users/{username}/code-challenges/completed")
    fun getCompletedChallenges(@Path("username") username: String, @Query("page") page: Int) : Observable<CompletedChallengesList>

    @GET("api/v1/users/{username}/code-challenges/authored")
    fun getAuthoredChallenges(@Path("username") username: String) : Observable<AuthoredChallengesList>

    companion object {
        private const val BASE_URL: String = "https://www.codewars.com"
        private const val AUTHORIZATION_NAME: String = "Authorization"
        private const val AUTHORIZATION_VALUE: String = "495TyzNWhvsqwsbXN6-B"

        fun getApi() : CodeWarsRestApi {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .header(AUTHORIZATION_NAME, AUTHORIZATION_VALUE)
                val request = requestBuilder.build()
                chain.proceed(request)
            }

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build().create(CodeWarsRestApi::class.java)
        }
    }
}