package com.ericcbm.corewars.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ericcbm.corewars.R
import com.ericcbm.corewars.ui.viewmodel.SearchMemberViewModel
import com.ericcbm.corewars.databinding.ActivityMainBinding
import com.ericcbm.corewars.model.Member
import com.ericcbm.corewars.ui.adapter.MemberListAdapter
import com.ericcbm.corewars.ui.viewmodel.ListMemberViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var searchMemberViewModel: SearchMemberViewModel
    private lateinit var listMemberViewModel: ListMemberViewModel
    private val memberListAdapter: MemberListAdapter = MemberListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView(this, R.layout.activity_main) as ActivityMainBinding
        binding.setLifecycleOwner(this)
        searchMemberViewModel = ViewModelProviders.of(this).get(SearchMemberViewModel::class.java)
        binding.viewModel = searchMemberViewModel
        content_main_member_recycler_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        listMemberViewModel = ViewModelProviders.of(this).get(ListMemberViewModel::class.java)
        listMemberViewModel.memberLits.observe(this, Observer { memberList ->
            memberListAdapter.updateMemberList(memberList)
        })
        content_main_member_recycler_view.adapter = memberListAdapter
        memberListAdapter.onItemClickListener = object :
            MemberListAdapter.OnItemClickLister {
            override fun onItemClick(member: Member) {
                val intent = Intent(this@MainActivity, ChallengesListActivity::class.java)
                intent.putExtra(ChallengesListActivity.EXTRA_USERNAME, member.username)
                startActivity(intent)
            }
        }
        listMemberViewModel.loadMembers()

        setSupportActionBar(toolbar)
        setUpViewListeners()
        setUpObservers()
    }

    private fun setUpObservers() {
        searchMemberViewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                Snackbar.make(fab, errorMessage, Snackbar.LENGTH_LONG).show()
            }
        })
        searchMemberViewModel.savedMember.observe(this, Observer {
            listMemberViewModel.loadMembers()
        })
        listMemberViewModel.orderedType.observe(this, Observer { orderedType ->
            if (orderedType != null) {
                val stringId =
                    if (orderedType == ListMemberViewModel.RANK_ORDERED_TYPE)
                        R.string.rank_ordered_messege
                    else R.string.search_ordered_messege
                Snackbar.make(fab, stringId, Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun setUpViewListeners() {
        content_main_search_edit_text.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    content_main_search_button.performClick(); true
                }
                else -> false
            }
        }
        content_main_search_button.setOnClickListener {
            searchMemberViewModel.searchMember(content_main_search_edit_text.text.toString())
        }
        fab.setOnClickListener {
            listMemberViewModel.changeOrder()
        }
    }
}
