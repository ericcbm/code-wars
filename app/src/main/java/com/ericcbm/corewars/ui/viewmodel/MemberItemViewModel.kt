package com.ericcbm.corewars.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ericcbm.corewars.model.Member

class MemberItemViewModel : ViewModel() {

    val member = MutableLiveData<Member>()

    fun bind(member: Member){
        this.member.value = member
    }

}