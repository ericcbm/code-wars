package com.ericcbm.corewars.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ericcbm.corewars.R
import com.ericcbm.corewars.databinding.ActivityChallengesListBinding
import com.ericcbm.corewars.model.Challenge
import com.ericcbm.corewars.ui.viewmodel.SearchChallengesViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_challenges_list.*
import com.ericcbm.corewars.ui.EndlessRecyclerViewScrollListener
import com.ericcbm.corewars.ui.adapter.ChallengesListAdapter

class ChallengesListActivity : AppCompatActivity() {

    private lateinit var username: String
    private lateinit var searchChallengesViewModel: SearchChallengesViewModel
    private val challengesListAdapter: ChallengesListAdapter = ChallengesListAdapter()

    companion object {
        const val EXTRA_USERNAME: String = "EXTRA_USERNAME"
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        challenges_recycler_view.scrollToPosition(0)
        challengesListAdapter.updateChallengeList(ArrayList())
        when (item.itemId) {
            R.id.completed_challenges -> {
                searchChallengesViewModel.getCompletedChallenges(username)
                return@OnNavigationItemSelectedListener true
            }
            R.id.authored_challenges -> {
                searchChallengesViewModel.getAuthoredChallenges(username)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView(this, R.layout.activity_challenges_list) as ActivityChallengesListBinding
        binding.setLifecycleOwner(this)

        username = intent.getStringExtra(EXTRA_USERNAME)

        val actionBar = supportActionBar
        actionBar!!.title = getString(R.string.title_activity_challenges_list, username)

        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        challenges_recycler_view.layoutManager = linearLayoutManager
        searchChallengesViewModel = ViewModelProviders.of(this).get(SearchChallengesViewModel::class.java)
        binding.viewModel = searchChallengesViewModel

        searchChallengesViewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                Snackbar.make(challenges_recycler_view, errorMessage, Snackbar.LENGTH_LONG).show()
            }
        })
        searchChallengesViewModel.challengesList.observe(this, Observer { challengesList ->
            if (challengesList != null) {
                if (challengesList.second) {
                    challengesListAdapter.appendChallengeList(challengesList.first)
                } else {
                    challengesListAdapter.updateChallengeList(challengesList.first)
                }
            }
        })

        challenges_recycler_view.adapter = challengesListAdapter
        challengesListAdapter.onItemClickListener = object :
            ChallengesListAdapter.OnItemClickLister {
            override fun onItemClick(challenge: Challenge) {
                val intent = Intent(this@ChallengesListActivity, ChallengeDetailActivity::class.java)
                intent.putExtra(ChallengeDetailActivity.EXTRA_CHALLENGE, challenge)
                startActivity(intent)
            }
        }
        challenges_recycler_view.addOnScrollListener(object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                if (nav_view.selectedItemId == R.id.completed_challenges) {
                    searchChallengesViewModel.getCompletedChallenges(username, page)
                }
            }
        })

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        nav_view.selectedItemId = R.id.completed_challenges
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
