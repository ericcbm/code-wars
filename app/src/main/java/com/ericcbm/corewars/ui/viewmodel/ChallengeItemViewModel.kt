package com.ericcbm.corewars.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ericcbm.corewars.model.Challenge

class ChallengeItemViewModel : ViewModel() {

    val challenge = MutableLiveData<Challenge>()

    fun bind(challenge: Challenge){
        this.challenge.value = challenge
    }

}