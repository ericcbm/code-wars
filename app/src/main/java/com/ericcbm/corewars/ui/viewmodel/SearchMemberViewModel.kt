package com.ericcbm.corewars.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ericcbm.corewars.CodeWarsRestApi
import com.ericcbm.corewars.database.CodeWarsDatabase
import com.ericcbm.corewars.model.Member
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SearchMemberViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val savedMember: MutableLiveData<Member> = MutableLiveData()
    private lateinit var searchMemberSubscription: Disposable
    private lateinit var saveMemberSubscription: Disposable
    private var codeWarsRestApi: CodeWarsRestApi =
        CodeWarsRestApi.getApi()

    fun searchMember(username : String) {
        searchMemberSubscription = codeWarsRestApi.getMember(username)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onAsyncProcessStart() }
            .doOnTerminate { onAsyncProcessFinish() }
            .subscribe( {result -> onMemberSearchResponse(result)}, { error -> onMemberSearchError(error) } )
    }

    private fun onAsyncProcessStart(){
        isLoading.postValue(true)
        errorMessage.postValue(null)
    }

    private fun onAsyncProcessFinish(){
        isLoading.postValue(false)
    }

    private fun onMemberSearchResponse(member : Member){
        member.prepareToSave()
        saveMemberSubscription = CodeWarsDatabase.getDatabase(getApplication()).memberDao().insert(member)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onAsyncProcessStart() }
            .doOnTerminate { onAsyncProcessFinish() }
            .subscribe( { onMemberSaveSuccess(member) },
                {error -> omMemberSaveError(error)} )
    }

    private fun onMemberSearchError(error: Throwable?){
        errorMessage.value = error?.localizedMessage
    }

    private fun omMemberSaveError(error: Throwable?) {
        errorMessage.postValue(error?.localizedMessage)
    }

    private fun onMemberSaveSuccess(member: Member) {
        savedMember.postValue(member)
    }

    override fun onCleared() {
        super.onCleared()
        searchMemberSubscription.dispose()
        saveMemberSubscription.dispose()
    }
}