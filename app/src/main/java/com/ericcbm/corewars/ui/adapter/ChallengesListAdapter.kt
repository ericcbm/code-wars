package com.ericcbm.corewars.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ericcbm.corewars.R
import com.ericcbm.corewars.databinding.ChallengeItemBinding
import com.ericcbm.corewars.model.Challenge
import com.ericcbm.corewars.ui.viewmodel.ChallengeItemViewModel

class ChallengesListAdapter: RecyclerView.Adapter<ChallengesListAdapter.ViewHolder>() {
    private lateinit var challengeList:MutableList<Challenge>

    var onItemClickListener: OnItemClickLister? = null

    interface OnItemClickLister {
        fun onItemClick(challenge: Challenge)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ChallengeItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.challenge_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(challengeList[position])
    }

    override fun getItemCount(): Int {
        return if(::challengeList.isInitialized) challengeList.size else 0
    }

    fun updateChallengeList(challengeList:List<Challenge>?){
        if (challengeList != null) {
            this.challengeList = challengeList.toMutableList()
        } else {
            this.challengeList = arrayListOf()
        }
        notifyDataSetChanged()
    }

    fun appendChallengeList(challengeList:List<Challenge>?){
        if (challengeList != null) {
            this.challengeList.addAll(challengeList)
        }
    }

    inner class ViewHolder(private val binding: ChallengeItemBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = ChallengeItemViewModel()

        fun bind(challenge: Challenge){
            binding.root.setOnClickListener {
                this@ChallengesListAdapter.onItemClickListener?.onItemClick(challenge)
            }
            viewModel.bind(challenge)
            binding.viewModel = viewModel
        }
    }
}