package com.ericcbm.corewars.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ericcbm.corewars.R
import com.ericcbm.corewars.model.Member
import com.ericcbm.corewars.ui.viewmodel.MemberItemViewModel
import com.ericcbm.corewars.databinding.MemberItemBinding

class MemberListAdapter: RecyclerView.Adapter<MemberListAdapter.ViewHolder>() {
    lateinit var memberList:List<Member>

    var onItemClickListener: OnItemClickLister? = null

    interface OnItemClickLister {
        fun onItemClick(member: Member)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MemberItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.member_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(memberList[position])
    }

    override fun getItemCount(): Int {
        return if(::memberList.isInitialized) memberList.size else 0
    }

    fun updateMemberList(memberList:List<Member>?){
        if (memberList != null) {
            this.memberList = memberList
        } else {
            this.memberList = ArrayList()
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: MemberItemBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = MemberItemViewModel()

        fun bind(member:Member){
            binding.root.setOnClickListener {
                this@MemberListAdapter.onItemClickListener?.onItemClick(member)
            }
            viewModel.bind(member)
            binding.viewModel = viewModel
        }
    }
}