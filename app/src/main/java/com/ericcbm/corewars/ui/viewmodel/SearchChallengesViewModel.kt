package com.ericcbm.corewars.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ericcbm.corewars.CodeWarsRestApi
import com.ericcbm.corewars.model.Challenge
import com.ericcbm.corewars.model.ChallengesList
import com.ericcbm.corewars.model.CompletedChallengesList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class SearchChallengesViewModel : ViewModel() {

    companion object {
        const val COMPLETED_TYPE: Int = 0
        const val AUTHORED_TYPE: Int = 1
    }

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isLoadingMore: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    //boolean value determines if appends the new list to the current one or not
    val challengesList: MutableLiveData<Pair<List<Challenge>?, Boolean>> = MutableLiveData()
    private var subscription: Disposable? = null
    private var codeWarsRestApi: CodeWarsRestApi =
        CodeWarsRestApi.getApi()

    fun getCompletedChallenges(username: String) {
        getCompletedChallenges(username, 0)
    }

    fun getCompletedChallenges(username: String, page: Int) {
        getChallenges(username, COMPLETED_TYPE, page)
    }

    fun getAuthoredChallenges(username: String) {
        getChallenges(username, AUTHORED_TYPE, 0)
    }

    private fun getChallenges(username: String, type: Int, page: Int) {
        subscription?.dispose()
        val observable: Observable<out ChallengesList> = if (type == COMPLETED_TYPE) {
            codeWarsRestApi.getCompletedChallenges(username, page)
        } else {
            codeWarsRestApi.getAuthoredChallenges(username)
        }
        subscription = observable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { onChallengesSearchStart(page) }
            .doOnTerminate { onChallengesSearchFinish(page) }
            .subscribe( {result -> onChallengesSearchResponse(result, page)},
                { error -> onChallengesSearchError(error) } )
    }

    private fun onChallengesSearchStart(page: Int){
        if (page > 0) {
            isLoadingMore.value = true
        } else {
            isLoading.value = true
        }
        errorMessage.value = null
    }

    private fun onChallengesSearchFinish(page: Int){
        if (page > 0) {
            isLoadingMore.value = false
        } else {
            isLoading.value = false
        }
    }

    private fun onChallengesSearchResponse(newChallengesList : ChallengesList, page: Int){
        challengesList.value = Pair(newChallengesList.data, page > 0)
        if (newChallengesList !is CompletedChallengesList || newChallengesList.totalPages == page + 1) {
            errorMessage.value = "Last page reached"
        }
    }

    private fun onChallengesSearchError(error: Throwable){
        errorMessage.value = error.localizedMessage
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }
}