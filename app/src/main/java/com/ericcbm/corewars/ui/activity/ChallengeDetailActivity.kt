package com.ericcbm.corewars.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ericcbm.corewars.R
import com.ericcbm.corewars.databinding.ActivityAuthoredChallengeDetailBinding
import com.ericcbm.corewars.databinding.ActivityCompletedChallengeDetailBinding
import com.ericcbm.corewars.model.AuthoredChallenge
import com.ericcbm.corewars.model.Challenge
import com.ericcbm.corewars.model.CompletedChallenge

class ChallengeDetailActivity : AppCompatActivity() {

    private lateinit var challenge: Challenge

    companion object {
        const val EXTRA_CHALLENGE: String = "EXTRA_CHALLENGE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ViewDataBinding
        challenge = intent.getParcelableExtra(EXTRA_CHALLENGE)
        if (challenge is AuthoredChallenge) {
            binding = DataBindingUtil.setContentView(
                this,
                R.layout.activity_authored_challenge_detail
            ) as ActivityAuthoredChallengeDetailBinding
            binding.authoredChallenge = challenge as AuthoredChallenge
        } else {
            binding = DataBindingUtil.setContentView(
                this,
                R.layout.activity_completed_challenge_detail
            ) as ActivityCompletedChallengeDetailBinding
            binding.completedChallenge = challenge as CompletedChallenge
        }
        binding.setLifecycleOwner(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
