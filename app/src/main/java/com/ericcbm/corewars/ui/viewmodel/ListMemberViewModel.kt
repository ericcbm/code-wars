package com.ericcbm.corewars.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ericcbm.corewars.database.CodeWarsDatabase
import com.ericcbm.corewars.model.Member
import com.ericcbm.corewars.ui.adapter.MemberListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ListMemberViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val RANK_ORDERED_TYPE: Int = 0
        const val SEARCH_ORDERED_TYPE: Int = 1
    }

    val orderedType: MutableLiveData<Int> = MutableLiveData()
    val memberLits: MutableLiveData<List<Member>> = MutableLiveData()
    private lateinit var subscription: Disposable

    init {
        orderedType.value = SEARCH_ORDERED_TYPE
    }

    fun changeOrder() {
        orderedType.value = if (orderedType.value == SEARCH_ORDERED_TYPE) RANK_ORDERED_TYPE else SEARCH_ORDERED_TYPE
        memberLits.value = getOrderedMembers(memberLits.value)
    }

    fun loadMembers(){
        val memberDao = CodeWarsDatabase.getDatabase(getApplication()).memberDao()
        subscription = memberDao.getFiveLastSearched()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {result -> onListMemberResponse(result)}, { error -> onListMemberError(error) } )

    }

    private fun onListMemberError(error: Throwable?) {
        error?.printStackTrace()
    }

    private fun onListMemberResponse(result: List<Member>?) {
        memberLits.value = getOrderedMembers(result)
    }

    private fun getOrderedMembers(result: List<Member>?) =
        if (orderedType.value == SEARCH_ORDERED_TYPE) result?.sortedByDescending { it.searchTimestamp }
        else result?.sortedBy { it.leaderboardPosition }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}