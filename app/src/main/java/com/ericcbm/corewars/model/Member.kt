package com.ericcbm.corewars.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.JsonObject

@Entity
data class Member(
    @field:PrimaryKey
    val username : String,
    val name : String?,
    var leaderboardPosition : Int?,
    var searchTimestamp: Long?,
    var ranks: JsonObject?,
    var bestLanguage: String?,
    var bestLanguageScore: Int?
) {

    fun getRank(): String {
        return if (leaderboardPosition == Int.MAX_VALUE) "#NONE" else "#$leaderboardPosition"
    }

    fun prepareToSave() {
        searchTimestamp = System.currentTimeMillis()
        bestLanguage = ""
        bestLanguageScore = 0
        val languages = ranks?.get("languages") as JsonObject
        for (languageName in languages.keySet()) {
            val score = (languages[languageName] as JsonObject)["score"].asInt
            if (score > bestLanguageScore!!) {
                bestLanguage = languageName
                bestLanguageScore = score
            }
        }
        if (leaderboardPosition == null) {
            leaderboardPosition = Int.MAX_VALUE
        }
    }
}
