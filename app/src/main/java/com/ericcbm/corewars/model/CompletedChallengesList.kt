package com.ericcbm.corewars.model

data class CompletedChallengesList(
    override val data: List<CompletedChallenge>?,
    val totalPages: Int?,
    val totalItems: Int?
): ChallengesList(data)