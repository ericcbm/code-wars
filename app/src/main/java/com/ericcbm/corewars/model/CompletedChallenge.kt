package com.ericcbm.corewars.model

import android.os.Parcel
import android.os.Parcelable

data class CompletedChallenge(
    override  val id : String?,
    override val name : String?,
    val slug: String?,
    val completedAt: String?,
    val completedLanguages: List<String>?
): Challenge(id, name) {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readArrayList(String::class.java.classLoader)?.filterIsInstance<String>()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(slug)
        parcel.writeString(completedAt)
        parcel.writeList(completedLanguages)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CompletedChallenge> {
        override fun createFromParcel(parcel: Parcel): CompletedChallenge {
            return CompletedChallenge(parcel)
        }

        override fun newArray(size: Int): Array<CompletedChallenge?> {
            return arrayOfNulls(size)
        }
    }

    fun getFormattedCompletedLanguages(): String {
        return completedLanguages!!.joinToString()
    }
}
