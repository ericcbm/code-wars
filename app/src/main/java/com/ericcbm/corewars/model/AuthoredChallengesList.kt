package com.ericcbm.corewars.model

data class AuthoredChallengesList(
    override val data: List<AuthoredChallenge>?,
    val totalPages: Int?,
    val totalItems: Int?
): ChallengesList(data)