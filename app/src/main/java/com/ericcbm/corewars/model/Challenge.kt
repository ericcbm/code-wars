package com.ericcbm.corewars.model

import android.os.Parcelable

abstract class Challenge(
    @Transient open val id : String?,
    @Transient open val name : String?
): Parcelable
