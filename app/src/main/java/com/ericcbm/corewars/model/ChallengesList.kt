package com.ericcbm.corewars.model

open class ChallengesList(
    @Transient open val data: List<Challenge>?
)