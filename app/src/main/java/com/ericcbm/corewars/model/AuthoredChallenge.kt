package com.ericcbm.corewars.model

import android.os.Parcel
import android.os.Parcelable

data class AuthoredChallenge(
    override val id : String?,
    override val name : String?,
    val description: String?,
    val rank: Int,
    val rankName: String?,
    val tags: List<String>?,
    val languages: List<String>?
): Challenge(id, name) {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readArrayList(String::class.java.classLoader)?.filterIsInstance<String>(),
        parcel.readArrayList(String::class.java.classLoader)?.filterIsInstance<String>()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(rank)
        parcel.writeString(rankName)
        parcel.writeList(tags)
        parcel.writeList(languages)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AuthoredChallenge> {
        override fun createFromParcel(parcel: Parcel): AuthoredChallenge {
            return AuthoredChallenge(parcel)
        }

        override fun newArray(size: Int): Array<AuthoredChallenge?> {
            return arrayOfNulls(size)
        }
    }

    fun getFormattedTags(): String {
        return tags!!.joinToString()
    }

    fun getFormattedLanguages(): String {
        return languages!!.joinToString()
    }
}
